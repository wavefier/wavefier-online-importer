import logging
import time
from wavefier_common.kafka.Producer import KafkaProducer
from confluent_kafka import Producer
from wavefier_common.RawData import RawData
import os

logging.basicConfig(level=logging.INFO)

class Watcher:
    """
        This class can be used to consume new files from online
    """
    def __init__(self):
        self.__dest_broker = os.environ['KAFKA_SOURCE_BROKER']
        self.__producer = KafkaProducer({'bootstrap.servers': self.__dest_broker})
        if self.__producer is None:
            logging.error("Error connecting with broker: %s", self.__dest_broker)
            return
        else:
            logging.info("Broker connected: %s", self.__dest_broker)

    def run(self):
        #f = open("frame/test.gwf", "rb")

        path = 'frame'

        print(os.getcwd())

        files = os.listdir(path)

        for name in files:
            logging.info("File: "+name)
            f = open("frame/"+name, "rb")
            self.publish_message_confluent(f.read())
            f.close()


    def publish_message(self, data):
        """
            Push on kafka the content of filename passed

            :param data: chuncks of data
        """
        logging.info("New file to push")
        message = RawData("ONLINE-0.gwf", data)
        self.__producer.publish_message(message)
        logging.info("Message published")

    def publish_message_confluent(self, data):
        print(self.__dest_broker)
        p = Producer({'bootstrap.servers': self.__dest_broker})


        def delivery_report(err, msg):
            """ Called once for each message produced to indicate delivery result.
                Triggered by poll() or flush(). """
            if err is not None:
                logging.info('Message delivery failed: {}'.format(err))
            else:
                logging.info('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

        # Trigger any available delivery report callbacks from previous produce() calls
        p.poll(0)

        # Asynchronously produce a message, the delivery report callback
        # will be triggered from poll() above, or flush() below, when the message has
        # been successfully delivered or failed permanently.
        p.produce('online', data, callback=delivery_report)

        # Wait for any outstanding messages to be delivered and delivery report
        # callbacks to be triggered.
        p.flush()



if __name__ == '__main__':
    time.sleep(1)
    w = Watcher()
    w.run()
