import logging
import os
from confluent_kafka import Consumer
from wavefier_common.kafka.Producer import KafkaProducer
from confluent_kafka.cimpl import KafkaError
from wavefier_common.RawData import RawData


class Watcher:
    """
        This class can be used to consume new files from online
    """
    def __init__(self):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
        self.__topics = ["online"]
        self.__source_broker = os.environ['KAFKA_SOURCE_BROKER']
        self.__consumer = Consumer({
            'bootstrap.servers': self.__source_broker,
            'group.id': 'onlineDataGroup',
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        self.__dest_broker = os.environ['KAFKA_DEST_BROKER']
        self.__producer = KafkaProducer({'bootstrap.servers': self.__dest_broker})
        if self.__producer is None:
            logging.error("Error connecting with broker: %s", self.__dest_broker)
            return
        else:
            logging.info("Broker connected: %s", self.__dest_broker)

    def run(self):
        self.__consumer.subscribe(self.__topics)
        i = 0
        while True:
            online_file = self.consume_message()

           # tmp_name = "data_" + str(i) + ".gwf"
           # with open(tmp_name, "wb") as g:
           #     g.write(online_file)

            self.publish_message(online_file)
           # i = i + 1

    def publish_message(self, data):
        """
            Push on kafka the content of filename passed

            :param data: chuncks of data
        """
        logging.debug("New file to push")
        message = RawData("online-0.gwf", data)
        self.__producer.publish_message(message)
        logging.info("Message published")


    def consume_message(self, timeout_in_seconds=None):
        """Consume a message from subscribed topics
        """
        eof_reached = []

        while True:
            if timeout_in_seconds is None:
                msg = self.__consumer.poll()
            else:
                msg = self.__consumer.poll(timeout_in_seconds)

            if msg is None:
                raise Exception('Got timeout from poll() without a timeout set: %s' % msg)

            if msg.error():
                logging.error('Message error:')
                logging.error(msg.error().code())
                break
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    logging.warning('Reached end of %s [%d] at offset %d' %
                          (msg.topic(), msg.partition(), msg.offset()))
                    eof_reached[(msg.topic(), msg.partition())] = True
                    if len(eof_reached) == len(self.__consumer.assignment()):
                        logging.warning('EOF reached for all assigned partitions: exiting')
                        break
                else:
                    logging.warning('Consumer error: %s: ignoring' % msg.error())
                    break
            else:
                logging.info("Message consumed")

            return msg.value()





if __name__ == '__main__':
    w = Watcher()
    w.run()
