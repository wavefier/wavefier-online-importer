************
Installation
************


Via Python Package
==================

Install the package (or add it to your ``requirements.txt`` file):

.. code:: bash

    pip install git+https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-online-importer.git@v0.0.1

In your ``conf.py`` file:

.. code:: python

    setup(
        ...

        install_requires=[
            ...,
            'wavefier-online-importer',
            ...
        ],

        ...

        dependency_links=[
            'git+https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-online-importer.git@v0.0.1#egg=wavefier-online-importer'
        ]

        ...
    )



Via Git or Download
===================

Clone the repository via canonical command line

.. code:: bash

    git clone https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-online-importer.git

Switch on prefered branch one has to run ``setup.py`` script from the main directory of the library.

As example you can run ``python setup.py install``