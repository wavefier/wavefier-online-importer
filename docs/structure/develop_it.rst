***************************
How to develop this modules
***************************



Is it possible to develop this library via docker image, is just required to have docker daemon up and running.


Setup the Environment
=====================

First to all you need to clone the source and then you can start to create the develop environment:

.. code:: bash

    git clone https://ro:rorororo@gitlab.trust-itservices.com/wavefier/wavefier-online-importer.git

To create the environment  ( aka docker image) for the develop just run the command

.. code:: bash

    docker build -t wavefier/wavefier-online-importer .

Then start/run the environment bash with

.. code:: bash

    docker run -it -v $(pwd):/app wavefier/wavefier-online-importer bash

From now on, a totally autonomous development environment with all dependency and the right version of python are available.


Some example of command
=======================

Below a example of command that you can run on


Make Documentation
------------------
To create documentation use the ``python setup.py build_sphinx``. The project use ``sphinx`` for the generation of documentation. On the directory docs you can see the generated documentation

.. admonition:: And, by the way... Sphinx allow other options:

    - ``python setup.py build_sphinx -b epub``
    - ``python setup.py build_sphinx -b latex``


Run Test
--------
To run tests use ``python setup.py nosetests``. The project use ``nose`` for the automatic test.

.. admonition:: And, by the way...

    For the test please remember to setup all the test dependencies, **before** to run the test. Is more useful if you, outside the develop shell, perform the the follow command:

    .. code:: bash

        ### Setup Test Environment
        cd tools/kafka
        docker-compose up -d
        cd ../..

        ### Run all the Test
        docker run --network=kafka_kafka_network_test wavefier/wavefier-online-importer python setup.py nosetests

        ### Shutdown the test Environment
        cd tools/kafka
        docker-compose down
        cd ../..


Create Source dist build
------------------------
To create a source distribution, you run: ``python setup.py sdist``. It create a tar gz on a directory dist with a source distribution

Create Binary dist build
------------------------
To create a binary distribution called a wheel, you run: ``python setup.py bdist``

.. admonition:: And, by the way...
    Other options can be used:

    - ``python setup.py bdist --formats=rpm`` rpm Format
    - ``python setup.py bdist --formats=wininst`` window installer
