FROM wavefier/python:3.6.8-jessie

WORKDIR /importer

COPY .  /importer/.

RUN python setup.py install

CMD ["python", "-u", "wavefier_online_importer/ConsumerFiles.py"]
