# Wavefier Online Importer
The purpose of this module is to create a mechanism to importe files within the **Wavefier** pipeline. 
Using Wavefier Online Importer it is possible to read from soruce KAFKA broker files (1sec) and store into the Wavefier pipeline (putting them in internal KAFKA broker).

It is possible exploit 'SourcePusher.py' to read from local filesystem and push it into source KAFKA broker

It is possible configure the module via **docker-compose** exploiting environment variables:


* **DIR_MOUNT** Indicate the directory to watch in order to trigger new event
* **KAFKA_SOURCE_BROKER** Indicate the address of source KAFKA broker to reach the internal Wavefier environment
* **KAFKA_DEST_BROKER** Indicate the address of destination KAFKA broker to put the file into internal Wavefier pipeline
