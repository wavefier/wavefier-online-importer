from setuptools import setup, find_packages

setup(
    name='wavefier_online_importer',
    version='2.0.0',
    packages=find_packages(exclude=[
            'build',
            'docs',
            'tests',
            'tools',
        ]),
    url='',
    license='',
    author='Emanuel Marzini',
    author_email='e.marzini@commpla.com',
    description='Library to import files in kafka',
    long_description=open('README.md').read(),
    test_suite='nose.collector',
    setup_requires=['nose>=1.0'],
    install_requires=[
        'wavefier-common==2.0.0',
        'watchdog'],
    extras_require={':python_version == "2.7"': ['futures']},
    dependency_links=[
        "git+https://gitlab+deploy-token-661042:fb4XV93yiAjJXsT3K3f2@gitlab.com/wavefier/wavefier-common.git#egg=wavefier-common-2.0.0"
    ]
)
